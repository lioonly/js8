// //Найти все параграфы на странице.

// //Задание 1
// let pargagraphs = document.getElementsByTagName("p");

// for(el in pargagraphs) {
//     console.log(pargagraphs[el])
//     pargagraphs[el].style.backgroundColor = 'red';
// }

// //Задание 2
// const optionsList = document.getElementById("optionsList");
// console.log(`Дочерние елементы: `, optionsList.childNodes)
// console.log(`Родительский елемент: `, optionsList.parentNode)

// //Задание 3
// const testParagraph = document.getElementById("testParagraph");
// testParagraph.innerHTML = '<p>This is a paragraph</p>'
// console.log(testParagraph)

// //Задание 4
// const mainHeader = document.getElementsByClassName('main-header')[0];
// const allElems = mainHeader.getElementsByTagName("li");

// for(el in allElems) {
//     allElems[el].classList.add("nav-item")
// }

// //Задание 5
// const sectionTitles = document.getElementsByClassName("section-title")[0];

// sectionTitles.classList.remove("section-title");